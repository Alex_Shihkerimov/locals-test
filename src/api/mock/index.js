import persons from './data/persons'

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockData)
    }, time)
  })
}

export default {
  fetchPersons() {
    return fetch(persons, 1000) // wait 1s before returning persons
  }
}
