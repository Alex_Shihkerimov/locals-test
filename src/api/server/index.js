import axios from 'axios'

export default {
  fetchPersons() {
    return axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then(response => response.data)
  }
}
