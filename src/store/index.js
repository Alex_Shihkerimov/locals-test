import Vue from 'vue'
import Vuex from 'vuex'
import persons from './persons'
import auth from './auth'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    persons
  }
})
