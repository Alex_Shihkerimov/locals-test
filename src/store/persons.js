import client from 'api-client'

export default {
    state: {
        persons: []
    },

    mutations: {
        setPersons(state, persons) {
            state.persons = persons
        }
    },

    actions: {
        fetchPersons({ commit }) {
            return client
                .fetchPersons()
                .then(persons => commit('setPersons', persons))
        }
    },
    getters: {
        persons(state) {
            return state.persons
        }
    }
}