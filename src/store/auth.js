//user login state

export default {
    state: {
        user: null
    },

    mutations: {
        setUser(state, user) {
            state.user = user
        }
    },

    actions: {
        loginUser({ commit }, { user }) {
            commit('setUser', user)
        },
        autoLoginUser({ commit }, user) { // for reload page
            commit('setUser', user)
        },
        logoutUser({ commit }) {
            commit('setUser', null)
        }
    },
    getters: {
        authState(state) {
            return state.user
        },
        isUserLoggedIn(state) {
            return state.user !== null
        }
    }
}