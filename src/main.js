import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  created() {
    if (localStorage.user && this.$router.currentRoute !== 'login') {
      this.$store.dispatch('autoLoginUser', localStorage.user)
    }
  },
  render: h => h(App)
}).$mount('#app')
